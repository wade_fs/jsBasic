package com.wade.jsbasic;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Main extends AppCompatActivity {
    private final static String TAG="MyLog";
    private final static String JavaBind ="Android";
    private Main context;

    WebView myBrowser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        context = this;
        copyWWW();

        myBrowser = (WebView)findViewById(R.id.webview);
        setBrowser();
//        myBrowser.loadUrl("http://localhost:8888/index.html");
//        myBrowser.loadUrl("file:///storage/sdcard0/jsbasic/index.html");
        myBrowser.loadUrl("file://"+Environment.getExternalStorageDirectory()+"/jsbasic/index.html");
        ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    private boolean checkWWW() {
        File www = new File(Environment.getExternalStorageDirectory()+"/jsbasic/index.html");
        File jsbasic = new File(Environment.getExternalStorageDirectory()+"/jsbasic/cm/basic.js");
        if (www.exists() && jsbasic.exists()) return true;
        else return false;
    }
    private void copyWWW() {
        if (!checkWWW()) {
            warn("需要花點時間安裝資料檔，請耐心等候...", 2);
            try {
                InputStream _inputStream = getAssets().open("jsbasic.zip");
                String outpath = Environment.getExternalStorageDirectory().getPath();
                try  {
                    ZipInputStream zin = new ZipInputStream(_inputStream);
                    ZipEntry ze = null;
                    while ((ze = zin.getNextEntry()) != null) {
                        warn("解壓項目:"+ze.getName(), 1);
                        if(ze.isDirectory()) {
                            _dirChecker(outpath+"/"+ze.getName());
                        } else {
                            FileOutputStream fout = new FileOutputStream(outpath+"/" + ze.getName());
                            byte[] buf = new byte[1024];
                            for (int c = zin.read(buf); c != -1; c = zin.read(buf)) {
                                fout.write(buf, 0, c);
                            }

                            zin.closeEntry();
                            fout.close();
                        }
                    }
                    zin.close();
                    warn("恭喜！資料檔安裝完成", 0);
                } catch(Exception e) {
                    warn("複製檔案失敗:" + e.toString(), 1);
                }
            } catch (FileNotFoundException e) {
                warn("jsbasic.zip 不存在", 1);
            } catch (IOException e) {
                warn("無法找到 jsbasic.zip 資源檔: "+e.toString(), 1);
            }
        }
    }
    private void _dirChecker(String dir) {
        File f = new File(dir);
        f.mkdirs();
    }
    private void setBrowser() {
        myBrowser.addJavascriptInterface(new WebAppInterface(this), JavaBind);
        WebSettings mWebSettings = myBrowser.getSettings();
        mWebSettings.setJavaScriptEnabled(true);
        mWebSettings.setAllowFileAccessFromFileURLs(true);
        mWebSettings.setAllowUniversalAccessFromFileURLs(true);
        mWebSettings.setBuiltInZoomControls(true);
        mWebSettings.setDefaultFontSize(14);
        myBrowser.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        myBrowser.setScrollbarFadingEnabled(false);
        myBrowser.setVerticalScrollBarEnabled(true);
    }

    public void warn(String msg, int mode) {
        if (mode != 2) Log.d("MyLog", msg);
        if (mode != 1) Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
    // 底下是從 javascript 要往 java 呼叫函數時使用，
    // 在 javascript 中大約等於 Android.XXX(), where XXX 定義在底下
    //
    // 如果要從 APK 來呼叫 JavaScript, 則簡單的透過 myBrowser.loadUrl("javascript:YYY(....)");
    // 其中 YYY 定義在 javascript 中
    public class WebAppInterface {
        Context context;
        WebAppInterface(Context c) {
            context = c;
        }

        @JavascriptInterface
        public void showKeyboard() {
            ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
        @JavascriptInterface
        public void showToast(String toast) {
            warn(toast, 0);
        }
        private List<List<String>> loadApps() {
            File file = new File(Environment.getExternalStorageDirectory().getPath()+"/jsbasic/apps.js");
            List<List<String>> maps = new ArrayList<>();
            if (file.exists()) {
                try {
                    FileInputStream is = new FileInputStream(file);
                    int size = is.available();
                    byte[] buffer = new byte[size];
                    is.read(buffer);
                    is.close();
                    String json = new String(buffer, "UTF-8");
                    json = json.substring(json.indexOf("\n"));
                    JSONArray apps = null;
                    apps = new JSONArray(json);
                    for (int i = 0; i < apps.length(); i++) {
                        Object o = apps.get(i);
                        if (o != null) {
                            JSONArray item = apps.getJSONArray(i);
                            maps.add(Arrays.asList(
                                    item.get(0).toString(), item.get(1).toString()
                            ));
                        }
                    }
                } catch (FileNotFoundException e) {
                    warn("Cannot access " + file.getPath(), 0);
                } catch (IOException e) {
                    warn("Cannot read " + file.getPath(), 0);
                } catch (JSONException e) {
                }
            }
            return maps;
        }
        private void saveApps(List<List<String>> maps) {
            File file = new File(Environment.getExternalStorageDirectory().getPath()+"/jsbasic/apps.js");
            String res = "var apps =\n[\n";
            for (List<String> item : maps) {
                res += "  [\""+item.get(0)+"\",\""+item.get(1)+"\"],\n";
            }
            res = res.substring(0,res.length()-2);
            res += "\n];";

            try {
                FileOutputStream os = new FileOutputStream(file);
                os.write(res.getBytes());
                os.close();
            } catch (FileNotFoundException e) {

            } catch (IOException e) {
            }
        }
        private void addApp(String showName, String fileName) {
            File file = new File(fileName);
            if (file.exists()) {
                List<List<String>> maps = loadApps();
                boolean same = false;
                fileName = fileName.substring(fileName.lastIndexOf("/")+1, fileName.lastIndexOf("."));
                for (List<String> a : maps) {
                    if (a.get(0).equals(fileName)) {
                        same = true;
                        a.set(1, showName);
                    }
                }

                if (!same) {
                    maps.add(Arrays.asList(fileName, showName));
                }
                saveApps(maps);
            }
        }
        private void removeFile(String fileName) {
            File file = new File(Environment.getExternalStorageDirectory().getPath()+"/jsbasic/jsbasic/"+fileName+".txt");
            file.delete();
        }
        private int w = -1;
        private Drawable llBG = null;
        private List<TextView> tvs;
        @JavascriptInterface
        public void openDeleteDialog(){
            final List<List<String>> maps = loadApps();
            tvs = new ArrayList<>();

            ScrollView sv = new ScrollView(context);
            LinearLayout ll = new LinearLayout(context);
            ll.setOrientation(LinearLayout.VERTICAL);

            w = -1;

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
            for (int i=0; i<maps.size(); i++) {
                final int id = i;
                LinearLayout ll1 = new LinearLayout(context);
                ll1.setOrientation(LinearLayout.HORIZONTAL);

                TextView tv = new TextView(context);
                tv.setText(maps.get(i).get(1));
                tv.setLayoutParams(params);
                tv.setGravity(Gravity.CENTER);
                tv.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (w >= 0) {
                            tvs.get(w).setBackground(llBG);
                            w = -1;
                        } else {
                            llBG = v.getBackground();
                        }
                        v.setBackgroundColor(Color.CYAN);
                        w = id;
                        return false;
                    }
                });
                ll1.addView(tv);
                tvs.add(tv);
                ll.addView(ll1);
            }
            sv.addView(ll);
            AlertDialog myDialog = new AlertDialog.Builder(context)
                    .setTitle("選擇想刪除的檔案")
                    .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setPositiveButton("刪除", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (w >= 0) {
                                final String fileName = maps.get(w).get(0);
                                removeFile(fileName);
                                maps.remove(w);
                                saveApps(maps);
                                myBrowser.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        myBrowser.loadUrl("javascript:adjustApps()");
                                    }
                                });
                                dialog.dismiss();
                            }
                        }
                    })
                    .setView(sv).show();
        }
        @JavascriptInterface
        public void openSaveDialog(String sourceCode){
            LinearLayout ll = new LinearLayout(context);
            View mylayout = getLayoutInflater().inflate(R.layout.savefile, null);
            ll.addView(mylayout);
            final String code = sourceCode;

            AlertDialog.Builder builder = new AlertDialog.Builder(context)
                    .setTitle("請填入存檔資訊")
                    .setView(ll)
                    .setPositiveButton("儲存", null)
                    .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

            Point size = new Point();
            getWindowManager().getDefaultDisplay().getSize(size);
            final AlertDialog myDialog = builder.create();
            myDialog.getWindow().setLayout(size.x, size.y);

            myDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    final DialogInterface _dialog = dialog;
                    Button btOk = ((AlertDialog)dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                    btOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            EditText etShowName = (EditText)((AlertDialog)_dialog).findViewById(R.id.showname);
                            EditText etFileName = (EditText)((AlertDialog)_dialog).findViewById(R.id.filename);
                            if (etShowName.getText().length() == 0) {
                                warn("請務必填入欲顯示的名稱", 0);
                            }
                            else if (etFileName.getText().length() == 0) {
                                warn("請務必填入欲儲存的檔案名稱", 0);
                            }
                            else {
                                String showName = etShowName.getText().toString();
                                String fileName = etFileName.getText().toString();
                                File file = new File (Environment.getExternalStorageDirectory().getPath() +
                                        "/jsbasic/jsbasic/"+fileName+".txt");
                                FileOutputStream f;
                                try {
                                    f = new FileOutputStream(file);
                                    f.write(code.getBytes());
                                    f.close();
                                    // TODO save()
                                    addApp(showName, file.getPath());
                                    myBrowser.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            myBrowser.loadUrl("javascript:adjustApps()");
                                        }
                                    });
                                } catch (FileNotFoundException e) {
                                } catch (IOException e) {
                                }
                                _dialog.dismiss();
                            }
                        }
                    });
                }
            });
            myDialog.show();
        }
    }
}

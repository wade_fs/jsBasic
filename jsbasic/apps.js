var apps =
[
  [ "02_traverse"		, "導線法計算" ],
  [ "03_intersection"		, "交會法與距離" ],
  [ "04_azimuth_distance"	, "方位角與距離" ],
  [ "05_coordinate_adj"		, "坐標統一計算" ],
  [ "06_side3"			, "內角換算" ],
  [ "07_triangulation_survey"	, "三角測量" ],
  [ "08_side2angle"		, "三邊測量" ],
  [ "09_p1_anti_intersection"	, "一點反交會" ],
  [ "10_p2_anti_intersection"	, "二點反交會" ],
  [ "11_p3_anti_intersection"	, "三點反交會" ],
  [ "12_celestial_body"		, "天體觀測" ],
];

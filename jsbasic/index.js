function adjustApps() {
  $('#lb_files').find('option').remove().end().append('<option disabled selected="selected">&#x1F4C1 我的程式</option>');
  loadApps();
}
function loadApps() {
  $.getScript("apps.js")
    .done(function() {
      $.each(apps, function (i, item) {
          $('#lb_files').append($('<option>', { 
            value: item[0],
            text : item[1] 
          }));
      });
    })
    .fail(function() {
      //Android.showToast("load apps.js fail");
    });
}
$(document).ready(function() {
  var s$ = function(s) { return document.querySelector(s); };
  var showSource = 1;
  //Android.showKeyboard();
  $('#sourceFrame').show();
  $('#frame').hide();

  loadApps();
  $('#btn_edit').click(function(e) {
    e.preventDefault();
    if (showSource == 0) {
      showSource = 1;
      $('#frame').hide();
      $('#sourceFrame').show();
    } else {
      showSource = 0;
      $('#frame').show();
      $('#sourceFrame').hide();
    }
    //Android.showKeyboard();
  });

  s$('#lb_files').selectedIndex = 0;
  s$('#lb_samples').selectedIndex = 0;

  var frame = s$('#frame');

  var keyboard = frame;
  var tty = new TTY(s$('#screen'), keyboard);
  (function() {
    // Install output hook for bell
    var b = new Bell(/^.*\/|/.exec(window.location)[0]);
    var orig = tty.writeChar;
    tty.writeChar = function index_writeChar(c) {
      if (c.charCodeAt(0) === 7)
        b.play();
      else
        orig(c);
    };
  }());
  var dos = new DOS(tty);

  var lores = new LoRes(s$('#lores'), 40, 48);
  var hires = new HiRes(s$('#hires'), 280, 192);
  var hires2 = new HiRes(s$('#hires2'), 280, 192);
  var display = {
    state: { graphics: false, full: true, page1: true, lores: true },
    setState: function(state, value /* ... */) {
      var args = Array.prototype.slice.call(arguments);
      while (args.length) {
        state = args.shift();
        value = args.shift();
        this.state[state] = value;
      }

      if (this.state.graphics) {
        lores.show(this.state.lores);
        hires.show(!this.state.lores && this.state.page1);
        hires2.show(!this.state.lores && !this.state.page1);
        tty.splitScreen(tty.getScreenSize().height - (this.state.full ? 0 : 4));
      } else {
        lores.show(false);
        hires.show(false);
        hires2.show(false);
        tty.splitScreen(0);
      }
    }
  };
  var pdl = [0, 0, 0, 0];

  // Lexical highlighting, if available
  var editor;
  if (typeof CodeMirror === 'function') {
    editor = new CodeMirror(s$('#editorframe'), {
      mode: 'basic',
      tabMode: 'default',
      content: s$('#source').value,
      height: '100%'
    });
  } else {
    editor = (function() {
      var textArea = document.createElement('textarea');
      s$('#editorframe').appendChild(textArea);
      textArea.style.width = '598px';
      textArea.style.height = '384px';
      return {
        getValue: function() {
          return textArea.value;
        },
        setValue: function(value) {
          textArea.value = value;
        },
        setCursor: function(line, column) {
          // TODO: Implement me!
        }
      };
    }());
  }

  function getSource() {
    return editor.getValue();
  }

  function setSource(source) {
    editor.setValue(source);
  }

  var program;
  s$('#btn_run').addEventListener('click', function(e) {
    e.preventDefault();
    showSource = 0;
    $('#frame').show();
    $('#sourceFrame').hide();

    dos.reset();
    tty.reset();
    tty.autoScroll = true;

    try {
      program = basic.compile(getSource());
    } catch (e) {
      if (e instanceof basic.ParseError) {
        editor.setCursor({ line: e.line - 1, ch: e.column - 1 });
        console.log(e.message +
                    ' (source line:' + e.line + ', column:' + e.column + ')');
      }
      alert(e);
      return;
    }

    stopped = false;
    updateUI();
    s$('#btn_stop').focus();

    program.init({
      tty: tty,
      hires: hires,
      hires2: hires2,
      lores: lores,
      display: display,
      paddle: function(n) { return pdl[n]; }
    });
    setTimeout(driver, 0);
    //Android.showKeyboard();
  });

  s$('#btn_stop').addEventListener('click', function(e) {
    e.preventDefault();

    tty.reset(); // cancel any blocking input
    stopped = true;
    updateUI();
  });

  s$('#lb_files').addEventListener('change', function() {
    var sel = s$('#lb_files');
    loadFile('jsbasic/' + sel.value + ".txt", setSource);
  });

  s$('#lb_samples').addEventListener('change', function() {
    var sel = s$('#lb_samples');
    loadFile('samples/' + sel.value + ".txt", setSource);
  });

  var current_file_name;
  s$('#btn_save').addEventListener('click', function(e) {
    e.preventDefault();
    //Android.openSaveDialog(getSource());
  });
  s$('#btn_delete').addEventListener('click', function(e) {
    e.preventDefault();
    //Android.openDeleteDialog();
  });

  // Mouse-as-Joystick
  var wrapper = s$('#screen-wrapper');
  wrapper.addEventListener('mousemove', function(e) {
    var rect = wrapper.getBoundingClientRect(),
        x = e.clientX - rect.left, y = e.clientY - rect.top;
    function clamp(n, min, max) { return n < min ? min : n > max ? max : n; }
    pdl[0] = clamp(x / (rect.width - 1), 0, 1);
    pdl[1] = clamp(y / (rect.height - 1), 0, 1);
  });

  var stopped = true;
  function updateUI() {
    var btnFocus = (document.activeElement === s$("#btn_run") ||
                    document.activeElement === s$("#btn_stop"));
    s$("#btn_stop").disabled = stopped ? "disabled" : "";
    s$("#btn_run").disabled = stopped ? "" : "disabled";
    s$("#lb_files").disabled = stopped ? "" : "disabled";
    s$("#lb_samples").disabled = stopped ? "" : "disabled";

    if (btnFocus || stopped) {
      s$(stopped ? "#btn_run" : "#btn_stop").focus();
    } else {
      tty.focus();
    }
    //Android.showKeyboard();
  }


  // TODO: Expose a RESPONSIVE |---+--------| FAST slider in the UI
  // Number of steps to execute before yielding execution
  // (Use a prime to minimize risk of phasing with loops)
  var NUM_SYNCHRONOUS_STEPS = 37;

  function driver() {
    var state = basic.STATE_RUNNING;
    var statements = NUM_SYNCHRONOUS_STEPS;

    while (!stopped && state === basic.STATE_RUNNING && statements > 0) {

      try {
        state = program.step(driver);
      } catch (e) {
        console.log(e);
        alert(e.message ? e.message : e);
        stopped = true;
        updateUI();
        return;
      }

      statements -= 1;
    }

    if (state === basic.STATE_STOPPED || stopped) {
      stopped = true;
      updateUI();
    } else if (state === basic.STATE_BLOCKED) {
      // Fall out
    } else { // state === basic.STATE_RUNNING
      setTimeout(driver, 0); // Keep going
    }
  }

  function parseQueryParams() {
    var params = {};
    var query = document.location.search.substring(1);
    query.split(/&/g).forEach(function(pair) {
      pair = pair.replace(/\+/g, " ").split(/=/).map(decodeURIComponent);
      params[pair[0]] = pair.length === 1 ? pair[0] : pair[1];
    });
    return params;
  }

  function loadFile(filename, callback) {
    var req = new XMLHttpRequest();
    var url = encodeURI(filename); // not encodeURIComponent, so paths can be specified
    var async = true;
    req.open("GET", url, async);
    req.onreadystatechange = function() {
      if (req.readyState === XMLHttpRequest.DONE) {
        if (req.status === 200 || req.status === 0) {
          callback(req.responseText);
        }
      }
    };
    req.send(null);
  }

  // load default
  var params = parseQueryParams();
  if ('source' in params) {
    setSource(params.source);
  } else {
    loadFile('jsbasic/default.txt', setSource);
  }
});
